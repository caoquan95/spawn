import numpy as np
from sklearn.utils import shuffle
from tensorflow.keras.utils import to_categorical, Sequence
import threading
import math


class threadsafe_generator(object):
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, gen):
        self.gen = gen
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.gen.__next__()


def threadsafe(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_generator(f(*a, **kw))
    return g


@threadsafe
def ftsdf_generator(file_prefixes, batch_size=4, shuff=False, shape=(240, 144, 240), down_scale = 4):  # write the definition of your data generator

    while True:
        x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))
        rgb_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 3))
        y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
        w_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale))
        batch_count = 0
        if shuff:
            file_prefixes_s = shuffle(file_prefixes)
        else:
            file_prefixes_s = file_prefixes

        for count, file_prefix in enumerate(file_prefixes_s):


            vox_tsdf, vox_rgb, segmentation_label, vox_weights = process(file_prefix, voxel_shape=(240, 144, 240), down_scale=4)

            x_batch[batch_count] = vox_tsdf.reshape((shape[0], shape[1], shape[2],1))
            rgb_batch[batch_count] = vox_rgb.reshape((shape[0], shape[1], shape[2],3))
            y_batch[batch_count] = to_categorical(segmentation_label.reshape((60, 36, 60,1)), num_classes=12)
            w_batch[batch_count] = vox_weights.reshape((60, 36, 60))
            batch_count += 1
            if batch_count == batch_size:
                yield [x_batch, rgb_batch], y_batch, w_batch
                x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1)) #channels last
                rgb_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 3)) #channels last
                y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
                w_batch = np.zeros((batch_size, shape[0]//down_scale, shape[1]//down_scale, shape[2]//down_scale))
                batch_count = 0

        if(batch_count > 0):
            yield [x_batch[:batch_count], rgb_batch[:batch_count]], y_batch[:batch_count], w_batch[:batch_count]


class PreprocSequence(Sequence):

    def __init__(self, file_prefixes, batch_size=4, shuff=False, vol=False, shape=(240, 144, 240), down_scale = 4, input_type="depth"):
        if shuff:
            self.file_prefixes = shuffle(file_prefixes)
        else:
            self.file_prefixes = file_prefixes

        self.batch_size = batch_size
        self.shape = shape
        self.down_shape = (shape[0] // down_scale,  shape[1] // down_scale, shape[2] // down_scale)
        self.vol = vol
        self.input_type = input_type

    def __len__(self):
        return int(np.ceil(len(self.file_prefixes) / self.batch_size))

    def __getitem__(self, idx):
        batch_files = self.file_prefixes[idx * self.batch_size:(idx + 1) * self.batch_size]

        if (self.input_type == "depth") or (self.input_type == "depth+edges"):
            depth_batch = np.zeros((len(batch_files), self.shape[0], self.shape[1], self.shape[2], 1))

        if (self.input_type == "edges") or (self.input_type == "depth+edges"):
            edges_batch = np.zeros((len(batch_files), self.shape[0], self.shape[1], self.shape[2], 1))

        y_batch = np.zeros((len(batch_files), self.down_shape[0], self.down_shape[1], self.down_shape[2], 12))

        if self.vol:
            vol_batch = np.zeros((len(batch_files), self.down_shape[0], self.down_shape[1], self.down_shape[2]))

        for batch_count, file_prefix in enumerate(batch_files):

            npz_file = file_prefix + '.npz'
            loaded = np.load(npz_file)

            if (self.input_type == "depth") or (self.input_type == "depth+edges"):
                vox_tsdf  = loaded['tsdf']
                depth_batch[batch_count] = vox_tsdf

            if (self.input_type == "edges") or (self.input_type == "depth+edges"):
                vox_edges = loaded['edges']
                edges_batch[batch_count] = vox_edges

            vox_label  = loaded['lbl']
            vox_label[vox_label==255] = 0
            vox_label[vox_label>11] = 11
            vox_weights = loaded['weights']
            if self.vol:
                vox_vol = loaded['vol']
                vol_batch[batch_count] = vox_vol

            labels = to_categorical(vox_label, num_classes=12)
            weights =  np.repeat(vox_weights,12,axis=-1).reshape((self.down_shape[0], self.down_shape[1], self.down_shape[2], 12))
            y_batch[batch_count] = labels * (weights+1)

        if self.input_type == "depth+edges":
            if self.vol:
                return [depth_batch, edges_batch], y_batch, vol_batch
            else:
                return [depth_batch, edges_batch], y_batch
        elif self.input_type == "depth":
            if self.vol:
                return depth_batch, y_batch, vol_batch
            else:
                return depth_batch, y_batch
        elif self.input_type == "edges":
            if self.vol:
                return edges_batch, y_batch, vol_batch
            else:
                return edges_batch, y_batch



#threadsafe
def preproc_generator(file_prefixes, batch_size=4, shuff=False, aug=False, vol=False, shape=(240, 144, 240), down_scale = 4, input_type="rgb"):  # write the definition of your data generator

    down_shape = (shape[0] // down_scale,  shape[1] // down_scale, shape[2] // down_scale)

    while True:

        if (input_type == "depth") or (input_type == "depth+edges"):
            depth_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))

        if (input_type == "edges") or (input_type == "depth+edges"):
            edges_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))

        y_batch = np.zeros((batch_size, down_shape[0], down_shape[1], down_shape[2], 12))

        if vol:
            vol_batch = np.zeros((batch_size, down_shape[0], down_shape[1], down_shape[2]))

        batch_count = 0

        if shuff:
            file_prefixes_s = shuffle(file_prefixes)
        else:
            file_prefixes_s = file_prefixes

        for count, file_prefix in enumerate(file_prefixes_s):

            npz_file = file_prefix + '.npz'
            try:
                loaded = np.load(npz_file)
            except:
                print("Bad npz:", npz_file)
                continue

            if (input_type == "depth") or (input_type == "depth+edges"):
                vox_tsdf  = loaded['tsdf']
                depth_batch[batch_count] = vox_tsdf

            if (input_type == "edges") or (input_type == "depth+edges"):
                vox_edges = loaded['edges']
                edges_batch[batch_count] = vox_edges

            vox_label  = loaded['lbl']
            vox_label[vox_label==255] = 0
            vox_label[vox_label>11] = 11
            vox_weights = loaded['weights']
            if vol:
                vox_vol = loaded['vol']

            if vol:
                vol_batch[batch_count] = vox_vol

            labels = to_categorical(vox_label, num_classes=12)
            weights =  np.repeat(vox_weights,12,axis=-1).reshape((down_shape[0], down_shape[1], down_shape[2], 12))
            y_batch[batch_count] = labels * (weights+1)
            batch_count += 1
            if batch_count == batch_size:
                if input_type == "depth+edges":
                    if vol:
                        yield [depth_batch, edges_batch], y_batch, vol_batch
                    else:
                        yield [depth_batch, edges_batch], y_batch
                elif input_type == "depth":
                    if vol:
                        yield depth_batch, y_batch, vol_batch
                    else:
                        yield depth_batch, y_batch
                elif input_type == "edges":
                    if vol:
                        yield edges_batch, y_batch, vol_batch
                    else:
                        yield edges_batch, y_batch

                if (input_type == "depth") or (input_type == "depth+edges"):
                    depth_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))  # channels last

                if (input_type == "edges") or (input_type == "depth+edges"):
                    edges_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))  # channels last

                y_batch = np.zeros((batch_size, down_shape[0], down_shape[1], down_shape[2], 12))

                if vol:
                    vol_batch = np.zeros((batch_size, down_shape[0], down_shape[1], down_shape[2]))

                batch_count = 0

        if batch_count > 0:
            if input_type == "depth+edges":
                if vol:
                    yield [depth_batch[:batch_count], edges_batch[:batch_count]], y_batch[:batch_count],vol_batch[:batch_count]
                else:
                    yield [depth_batch[:batch_count], edges_batch[:batch_count]], y_batch[:batch_count]
            elif input_type == "depth":
                if vol:
                    yield depth_batch[:batch_count], y_batch[:batch_count],vol_batch[:batch_count]
                else:
                    yield depth_batch[:batch_count], y_batch[:batch_count]
            elif input_type == "edges":
                if vol:
                    yield edges_batch[:batch_count], y_batch[:batch_count],vol_batch[:batch_count]
                else:
                    yield edges_batch[:batch_count], y_batch[:batch_count]

@threadsafe
def evaluate_generator(file_prefixes, batch_size=4, shuff=False, shape=(240, 144, 240), down_scale = 4):  # write the definition of your data generator

    while True:
        x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))
        y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
        f_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale),dtype=np.int32)
        batch_count = 0
        if shuff:
            file_prefixes_s = shuffle(file_prefixes)
        else:
            file_prefixes_s = file_prefixes

        for count, file_prefix in enumerate(file_prefixes_s):


            vox_tsdf, segmentation_label, vox_flags = process_evaluate(file_prefix, voxel_shape=(240, 144, 240), down_scale=4)

            x_batch[batch_count] = vox_tsdf.reshape((shape[0], shape[1], shape[2],1))
            y_batch[batch_count] = to_categorical(segmentation_label.reshape((60, 36, 60,1)), num_classes=12)
            f_batch[batch_count] = vox_flags.reshape((60, 36, 60))
            batch_count += 1
            if batch_count == batch_size:
                yield x_batch, y_batch, f_batch
                x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1)) #channels last
                y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
                f_batch = np.zeros((batch_size, shape[0]//down_scale, shape[1]//down_scale, shape[2]//down_scale),dtype=np.int32)
                batch_count = 0

        if(batch_count > 0):
            yield x_batch[:batch_count], y_batch[:batch_count],f_batch[:batch_count]

