import argparse
from utils.file_utils import get_file_prefixes_from_path
import json
import os
from utils import path_config, debug, obj
from suncg_utils import binvox, class_map, camera
import numpy as np
from matplotlib import path
from lib_edgenet.py_cuda import get_voxels, get_segmentation_class_map, lib_edgenet_setup, InTriangles
import time

##################################
# Parameters
##################################
TRAIN_TEST = 'test'  # test
SUFFIX = "none"

path_dict = path_config.read_config()

SUNCG_ROOT = path_dict["SUNCG_ROOT"]
RGBD_PATH = path_dict["RGBD_ROOT"]
CAM_PATH = path_dict["CAM_ROOT"]

cm = class_map.read_from_matlab()
seg_classes = get_segmentation_class_map()

def parse_arguments():
    global TRAIN_TEST, SUFFIX

    print("\nSemantic Scene Completion GT EXTRACTION Script\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset",       help="Train or test?", type=str, choices=['train','test'])
    parser.add_argument("--suffix",      help="Search suffix. Default " + str(SUFFIX),
                                         type=str, default=SUFFIX, required=False)
    args = parser.parse_args()

    SUFFIX = args.suffix
    TRAIN_TEST = args.dataset


def writeRLEfile(sceneVoxFilename, sceneVox, camPoseArr, voxOriginWorld):
    # Compress with RLE and save to binary file (first value represents how
    # many numbers are saved to the binary file)
    # Save vox origin in world coordinates as first three values

    sceneVoxArr =  sceneVox.flatten()
    sceneVoxGrad = sceneVoxArr[1:]-sceneVoxArr[: -1]
    sceneVoxPeaks = np.where(abs(sceneVoxGrad)>0)[0]

    if len(sceneVoxPeaks)>0:
        qtys = np.append(np.append([sceneVoxPeaks[0]+1],sceneVoxPeaks[1:]-sceneVoxPeaks[:-1]),len(sceneVoxArr)- sceneVoxPeaks[-1]-1)
        vals = np.append(sceneVoxArr[sceneVoxPeaks],sceneVoxArr[-1])
    else:
        qtys = np.array([len(sceneVoxArr)])
        vals = np.array([sceneVoxArr[0]])

    rle = np.column_stack((vals,qtys)).flatten().astype(np.uint32)

    with open(sceneVoxFilename, 'w') as f:
        voxOriginWorld.astype(np.float32).tofile(f)
        camPoseArr.astype(np.float32).tofile(f)
        rle.tofile(f)

def process_room(level_nodes, scene_id, floor_id, room_id, cameraPose, seq):


    #if scene_id[:5] != "e6a":
    #    return

    #if room_id !=0:
    #    return

    bin_file_sufix = "_fl" + str(floor_id+1).zfill(3) + "_rm" + str(room_id+1).zfill(4) + "_" + str(seq).zfill(6) + ".bin"

    os.makedirs(os.path.join(RGBD_PATH,'SUNCG',TRAIN_TEST,scene_id[:2]), exist_ok=True)

    bin_path = os.path.join(RGBD_PATH,'SUNCG',TRAIN_TEST,scene_id[:2], scene_id + bin_file_sufix)

    print("Processing %s ..." % bin_path, end=" " )

    if os.path.isfile(bin_path):
       print("Already exists!")
       return

    voxSizeCam = np.array([240,240,240])
    voxSize = np.array([240,240,144])
    voxSizeTarget = np.array([240,144,240])
    voxUnit = 0.02
    voxMargin = 5
    camK = np.array([[518.8579, 0, 320],
                     [0, 518.8579, 240],
                     [0, 0, 1]])
    height_belowfloor = -voxUnit
    im_w = 640
    im_h = 480

    extCam2World = camera.camPose2Extrinsics_zup(cameraPose)

    voxRangeExtremesCam = np.column_stack( (np.append(-voxSizeCam[0:2]*voxUnit / 2, 0),
                                            np.append(-voxSizeCam[0:2]*voxUnit / 2, 2) + voxSizeCam * voxUnit))

    voxOriginCam = np.mean(voxRangeExtremesCam, axis=1)
    #print("voxOriginCam",voxOriginCam)
    #print("cameraPose")
    #print(cameraPose)
    #print("extcam2World")
    #print(extCam2World[0:3, 0:3])

    # Compute voxel grid centers in world coordinates
    voxOriginWorld = np.transpose(extCam2World[0:3, 0:3]@np.transpose([voxOriginCam]))[0] + extCam2World[0:3,3] - voxSize/2*voxUnit

    room = level_nodes[room_id]
    node_id, node_type, model_id = room['id'], room['type'], room['modelId']

    room_min_bb_y = room['bbox']['min'][1]

    #print("room_min_bb_y",room_min_bb_y)

    voxOriginWorld[2] = room_min_bb_y - voxUnit
    #print("voxOriginWorld",voxOriginWorld)
    voxOriginWorld = np.round(voxOriginWorld,4)
    #print("voxOriginWorld",voxOriginWorld)
    values = np.arange(voxOriginWorld[2], voxOriginWorld[2] + 8 * voxUnit, voxUnit)
    #print("values", values)

    gridPtsWorldX, \
    gridPtsWorldY, \
    gridPtsWorldZ = np.meshgrid(np.arange(voxOriginWorld[0],voxOriginWorld[0]+(voxSize[0])*voxUnit, voxUnit),
                                np.arange(voxOriginWorld[1],voxOriginWorld[1]+(voxSize[1])*voxUnit, voxUnit),
                                np.arange(voxOriginWorld[2],voxOriginWorld[2]+(voxSize[2])*voxUnit, voxUnit))
    gridPtsWorld = np.column_stack([gridPtsWorldX.flatten(), gridPtsWorldY.flatten(), gridPtsWorldZ.flatten() ])
    gridPtsLabel = np.zeros(gridPtsWorld.shape[0], dtype=np.uint8)
    gridPtsWorld = np.round(gridPtsWorld/voxUnit)*voxUnit


    #Find Floor
    floor_file = os.path.join(SUNCG_ROOT, 'room', scene_id, model_id + 'f.obj')
    if not os.path.isfile(floor_file):
        print("No Floor!!!, skipping!")
        return None

    floorObj = obj.read(floor_file, colapse=True)
    min_x, max_x, min_y, max_y, min_z, max_z = floorObj.get_dim()

    floor_height = max_y


    #inRoom = np.zeros((gridPtsWorld.shape[0],), dtype=np.bool)
    #edges = floorObj.get_edges(floor_height)
    floor_polys=floorObj.get_face_polygons(floor_height)

    #print("floor_polys.shape", floor_polys.shape)
    #print(floor_polys[:2])
    #print("pts.shape", gridPtsWorld.shape)

    inRoom = InTriangles(floor_polys,gridPtsWorld[:,[0,1]].copy())

    #for floor_poly in floor_polys:
    #    floor_path = path.Path(floor_poly)
    #    inRoom |= floor_path.contains_points(gridPtsWorld[:,[0,1]])

    floorZ = round(max_y,4)

    #print("floorZ", floorZ)
    #print((values < np.round(floorZ+voxUnit/2,4)) & (values >= np.round(floorZ-3*voxUnit/2,4)))
    #print(np.round((values.astype(np.float32) - voxOriginWorld[2])/voxUnit))

    gridPtsObjWorldInd = inRoom & ((gridPtsWorld[:,2] <  np.round(floorZ+voxUnit/2,4)) & (gridPtsWorld[:,2] >= np.round(floorZ-3*voxUnit/2,4)))
    gridPtsLabel[gridPtsObjWorldInd] = class_map.classes_NYU['floor']

    gridPtsObjWorldInd = gridPtsWorld[:,2] < np.round(floorZ-3*voxUnit/2,4)
    gridPtsLabel[gridPtsObjWorldInd] = 255

    # find ceil
    ceil_file = os.path.join(SUNCG_ROOT, 'room', scene_id, model_id + 'c.obj')
    if os.path.isfile(ceil_file):
        ceilObj = obj.read(ceil_file, colapse=True)
        __, __, min_y_c, max_y_c, __, __ = ceilObj.get_dim()
        ceil_height = min_y_c

        ceilZ = min_y_c
        #ceilZ = (max_y_c + min_y_c) / 2

        #gridPtsObjWorldInd = inRoom & ( (gridPtsWorld[:,2] >= np.round(ceilZ-voxUnit/2,4)) & (gridPtsWorld[:,2] < np.round(ceilZ+3*voxUnit/2,4)))
        #gridPtsObjWorldInd = inRoom & (np.abs(gridPtsWorld[:,2]-ceilZ) <= voxUnit/2)

        gridPtsObjWorldInd = inRoom & ( (gridPtsWorld[:,2] >= np.round(ceilZ,4)) & (gridPtsWorld[:,2] <= np.round(ceilZ+2*voxUnit,4)))

        gridPtsLabel[gridPtsObjWorldInd] = class_map.classes_NYU['ceil']

        #gridPtsObjWorldInd = gridPtsWorld[:,2] > np.round(ceilZ +3*voxUnit/2,4)
        gridPtsObjWorldInd = gridPtsWorld[:,2] > np.round(ceilZ +2*voxUnit,4)
        gridPtsLabel[gridPtsObjWorldInd] = 255
    else:
        ceilZ = 0.0

    # walls
    wall_file = os.path.join(SUNCG_ROOT, 'room', scene_id, model_id + 'w.obj')
    if os.path.isfile(wall_file):
        wallObjs = obj.read(wall_file, colapse=False)
        wall_polys = np.empty(shape=(0,3,2), dtype=np.float32)
        #inWall = np.zeros((gridPtsWorld.shape[0],), dtype=bool)
        for wallObj in wallObjs:
            #if wallObj.name[:10] == "WallInside":
            a, b, __, wh, c, d = wallObj.get_dim()
            if ceilZ == 0.0:
                ceilZ = wh

            #edges = wallObj.get_edges(wh)
            faces = wallObj.get_face_polygons(wh)
            if len(faces)>0:
                wall_polys = np.append(wall_polys, wallObj.get_face_polygons(wh), axis=0)

        inWall =  InTriangles(wall_polys,gridPtsWorld[:,[0,1]].copy())

        #gridPtsObjWorldInd = inRoom & inWall & (gridPtsWorld[:,2]<=np.round(ceilZ,4))&(gridPtsWorld[:,2]>=np.round(floorZ+voxUnit/2,4))
        gridPtsObjWorldInd = inRoom & inWall & (gridPtsWorld[:,2]<=np.round(ceilZ,4))&(gridPtsWorld[:,2]>=np.round(floorZ+voxUnit/2,4))
        gridPtsLabel[gridPtsObjWorldInd] = class_map.classes_NYU['wall']

    # Remove grid points not in field of view
    extWorld2Cam = np.linalg.inv(np.append(extCam2World, [[0, 0, 0, 1]], axis=0))

    gridPtsCam = np.transpose(extWorld2Cam[0:3, 0:3] @ np.transpose(gridPtsWorld)) + extWorld2Cam[0: 3, 3]
    gridPtsPixX = (gridPtsCam[:, 0] * (camK[0, 0]) / gridPtsCam[:, 2]) + camK[0, 2]
    gridPtsPixY = (gridPtsCam[:, 1] * (camK[1, 1]) / gridPtsCam[:, 2]) + camK[1, 2]

    invalidPixInd = ((gridPtsPixX < 0) | (gridPtsPixX >= 640) | (gridPtsPixY < 0) | (gridPtsPixY >= 480))

    gridPtsLabel[invalidPixInd] = 255
    gridPtsLabel[(inRoom == False) & (gridPtsLabel==0)] = 255

    gt_grid = np.zeros(voxSizeTarget, dtype=np.uint8)  # empty

    [max_rwbx, max_rwby, max_rwbz] = room['bbox']['max']
    [min_rwbx, min_rwby, min_rwbz] = room['bbox']['min']

    #print("camera pos", cameraPose[:3])
    #print("vox origin", voxOriginWorld)
    #print("room min",room['bbox']['min'] )
    #print("room max",room['bbox']['max'] )

    for node_idx in range(len(level_nodes)):

        node = level_nodes[node_idx]

        if node['type'] != "Object":
            continue

        bbox = node['bbox']
        [max_wbx, max_wby, max_wbz] = bbox['max']
        [min_wbx, min_wby, min_wbz] = bbox['min']

        if not (((max_wbx < max_rwbx) and (max_wby < max_rwby) and (max_wbz < max_rwbz) and
                 (max_wbx > min_rwbx) and (max_wby > min_rwby) and (max_wbz > min_rwbz)) or
                ((min_wbx < max_rwbx) and (min_wby < max_rwby) and (min_wbz < max_rwbz) and
                 (min_wbx > min_rwbx) and (min_wby > min_rwby) and (min_wbz > min_rwbz))
                ):
               continue

        if 'modelId' in node:
            modelId = node['modelId'].replace("/","__")
            class_root = cm[modelId]['classroot_id']
            #print("%s obj %s   " % (scene_id, modelId), end='\r')
            binvox_path=os.path.join( SUNCG_ROOT,"object_vox","object_vox_data",modelId,modelId+".binvox")
            voxels, scale, translate = binvox.read(binvox_path)
            translate = translate[[0, 2, 1]]

            model_pts = (np.stack(np.where(voxels > 0),axis=-1)*scale)+translate
            if scale>voxUnit:
                mx = model_pts.copy()
                mx[:,0] = mx[:,0] + scale - voxUnit
                my = model_pts.copy()
                my[:,1] = my[:,1] + scale - voxUnit
                mz = model_pts.copy()
                mz[:,2] = mz[:,2] + scale - voxUnit
                mt = model_pts.copy()
                mt[:,:] = mt[:,:] + scale - voxUnit
                model_pts = np.append(np.append(np.append(np.append(model_pts, mx, axis=0),my, axis=0),mz, axis=0),mt, axis=0)

            # Convert object to world coordinates
            extObj2World_yup = np.reshape(node['transform'], [4, 4])
            model_pts = np.insert(model_pts,3,1,axis=-1)
            model_pts =  model_pts @  extObj2World_yup
            model_pts = model_pts[:,:-1]
            model_pts = model_pts[model_pts[:,1]>floorZ]


            #clear doors and windows bounding box
            if class_root == 4 or class_root == 5:
                #print("class_root", class_root, "modelid", modelId, cm[modelId]['classname'])
                # Get all grid points within the object bbox in world coordinates
                gridPtsObjWorldInd =  (gridPtsWorld[:,0] >= min_wbx - voxUnit/2) & (gridPtsWorld[:,0] <= max_wbx + voxUnit/2) & \
                                      (gridPtsWorld[:,2] >= min_wby - voxUnit/2) & (gridPtsWorld[:,2] <= max_wby + voxUnit/2) & \
                                      (gridPtsWorld[:,1] >= min_wbz - voxUnit/2) & (gridPtsWorld[:,1] <= max_wbz + voxUnit/2)

                gridPtsObjClearInd = gridPtsObjWorldInd  & (gridPtsLabel == class_map.classes_NYU['wall'])
                gridPtsLabel[gridPtsObjClearInd] = 0

            if len(model_pts)>0:
                model_labels = np.ones(len(model_pts), dtype=np.uint8) * class_root
                #antes = np.sum(gt_grid)
                get_voxels(model_pts[:,[0,2,1]], model_labels, gt_grid, voxSizeTarget, voxUnit, voxOriginWorld)
                #depois = np.sum(gt_grid)
                #if class_root==33 and antes==depois:
                #    print("model pts", model_pts[0])
                #    print("vox_origin", voxOriginWorld[[0,2,1]])
                #    print("\n\n", cm[modelId]['classname'], cm[modelId]['classroot_name'], class_root)
                #    print("room min", room['bbox']['min'])
                #    print("obj min", bbox['min'])
                #    print("obj max", bbox['max'])
                #    print("room max", room['bbox']['max'])


    gridPtsWorld = gridPtsWorld[gridPtsLabel > 0]
    gridPtsLabel = gridPtsLabel[gridPtsLabel > 0]

    if len(gridPtsWorld)>0:
        gt_grid = get_voxels(gridPtsWorld, gridPtsLabel, gt_grid, voxSizeTarget, voxUnit, voxOriginWorld)

    #voxOriginWorld[2] += height_belowfloor

    #gt_grid[:,1:,:] = gt_grid[:,:-1,:]

    camPoseArr = np.append(extCam2World,[0,0,0,1]) * np.array([[-1,1,1,1,-1,1,1,1,-1,1,1,1,-1,1,1,1]])
    writeRLEfile(bin_path, gt_grid, camPoseArr, voxOriginWorld)
    #gt = gt_grid.reshape(240, 144, 240)
    #print(0, "-", np.unique(gt[:, 0, :]))
    #print(1, "-", np.unique(gt[:, 1, :]))
    #print(2, "-", np.unique(gt[:, 2, :]))
    #print(3, "-", np.unique(gt[:, 3, :]))
    #print(4, "-", np.unique(gt[:, 4, :]))
    #print(5, "-", np.unique(gt[:, 5, :]))
    #print(6, "-", np.unique(gt[:, 6, :]))
    print("Done!!!")


def process_scene(scene_id):
    scene_path = os.path.join(SUNCG_ROOT, "house", scene_id, "house.json")

    with open(scene_path) as json_file:
        scene = json.load(json_file)

    debug.info("Json version: %s" % scene["version"])

    with open(os.path.join(CAM_PATH, TRAIN_TEST, scene_id[:2], scene_id + ".cam"), "r") as f:
        lines = f.readlines()
    cam_list = [np.array(line.split(),dtype=np.float32) for line in lines]

    with open(os.path.join(CAM_PATH, TRAIN_TEST, scene_id[:2], scene_id + ".caminfo"), "r") as f:
        lines = f.readlines()

    floor_list = [int(line.split("#")[1].split("_")[0]) for line in lines]
    room_list = [int(line.split("#")[1].split("_")[1]) for line in lines]

    for seq, (floor_id, room_id, camPose) in enumerate(zip(floor_list, room_list, cam_list)):
        level_nodes = scene['levels'][floor_id]['nodes']
        process_room(level_nodes, scene_id, floor_id, room_id, camPose, seq)

def process():
    print("CAM ROOT:", CAM_PATH)
    print("DATASET:", TRAIN_TEST)
    print("SUFFIX:", SUFFIX)

    file_prefixes = get_file_prefixes_from_path(os.path.join(CAM_PATH,TRAIN_TEST), criteria='*.caminfo')
    print("Total files:", len(file_prefixes))

    if SUFFIX != "none" and SUFFIX != "":
        qty = 0
        to_process = []
        for file_prefix in file_prefixes:
            base_name = os.path.basename(file_prefix)
            if base_name[:len(SUFFIX)] != SUFFIX:
                continue
            to_process.append(file_prefix)
    else:
        to_process = file_prefixes

    print("Files to process:", len(to_process))

    processed = 0

    start = time.time()

    for file_prefix in to_process:

        base_name = os.path.basename(file_prefix)
        sceneId = base_name[:32]

        #if sceneId != "e6bae95627b136b35015b055c062ce56":
        #    continue

        process_scene(sceneId)

        processed += 1
        print("%s - Elapsed: %4.2f Processed: %d (%.2f%%)\n" % (sceneId, time.time()-start, processed, 100 * processed / len(to_process)))

# Main Function
def Run():
    DEVICE=0
    lib_edgenet_setup(device=DEVICE, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24, debug=0)
    parse_arguments()
    process()


if __name__ == '__main__':
  Run()


