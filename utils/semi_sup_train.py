import numpy as np
import torch
import time
import copy
from tqdm import tqdm
from utils.metrics import Accuracy, MIoU
from torch.utils.tensorboard import SummaryWriter
from utils.misc import get_run
from utils.image import decode_outputs, decode_labels
from utils.data import sample2dev
import torch.nn.functional as F
from torch import optim

nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
               "empty"]

T1 = 5
T2 = 75


def eval(dataloader, weights):
    from tqdm import tqdm
    import os

    from utils.data import SSCMultimodalDataset, sample2dev
    from utils.data import get_file_prefixes_from_path
    from torch.utils.data import DataLoader
    import torch
    from utils.cuda import get_device
    import numpy as np
    from utils.metrics import MIoU, CompletionIoU
    from models.spawn import get_mmnet
    #import h5py
    import glob

    nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
                   "empty"]

    dev = get_device("cuda:1")
    torch.cuda.empty_cache()


    model = get_mmnet(input_type='depth+rgb', batch_norm=False, inst_norm=False)

    print("loading", weights)
    model.load_state_dict(torch.load(os.path.join("weights", weights)))

    model.to(dev)


    # Each epoch has a training and validation phase
    for phase in ['valid']:

        if phase == 'train':
            model.train()  # Set model to training mode
        else:
            model.eval()  # Set model to evaluate mode

        running_loss = 0.0

        # Iterate over data.
        tqdm_desc = "{}: MIoU: {:.5f}"
        num_samples = 0
        m_miou = MIoU(num_classes=12, ignore_class=0)

        with tqdm(total=len(dataloader), desc="") as pbar:
            for batch_num, sample in enumerate(dataloader):
                sample = sample2dev(sample, dev)

                vox_tsdf = sample['vox_tsdf']
                gt = sample['gt']

                if not ORACLE:
                    vox_prior = sample['vox_prior']
                else:
                    _, surf = torch.max(sample['vox_prior'], dim=1)
                    no_surf_idx = surf == 0
                    oracle_gt = gt.clone()
                    oracle_gt[no_surf_idx] = 0

                    # print("vp", sample['vox_prior'].shape)
                    # print("gt", gt.shape)
                    # print("oh_gt", torch.nn.functional.one_hot(gt.to(torch.int64), num_classes=12).shape)
                    vox_prior = torch.nn.functional.one_hot(oracle_gt.to(torch.int64), num_classes=12).to(
                        torch.float).transpose(3, 4).transpose(2, 3).transpose(1, 2)
                    # print("nvp",vox_prior.shape)

                vox_weights = sample['vox_weights']

                if torch.max(gt) > 11:
                    print("maior")
                    continue
                if torch.min(gt) < 0:
                    print("menor")
                    continue

                num_samples += gt.size(0)

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    # Special case for inception because in training it has an auxiliary output. In train
                    #   mode we calculate the loss by summing the final output and the auxiliary output
                    #   but in testing we only consider the final output.
                    outputs = model(vox_tsdf, vox_prior)

                    m_miou.update(outputs, gt, vox_weights)

                    # backward + optimize only if in training phase

                # statistics

                pbar.set_description(tqdm_desc.format(phase, m_miou.compute()) )

                pbar.update()


        if phase == 'valid':
            tb_text = ""
            for i in range(11):
                text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * m_miou.per_class_iou()[i])
                tb_text += text
                print(text, end="        ")
                if i % 4 == 3:
                    print()



def train_model(model, dev, dataloaders, unsup_dataloader, criterion, optimizer, num_epochs=25, patience=50,
                scheduler=None, suffix=None, alpha=.2, interval=5, oracle=False):
    run = get_run()
    time.sleep(.1)

    if suffix is None:
        model_name = "R{}_{}".format(run, type(model).__name__)
    else:
        model_name = "R{}_{}_{}".format(run, type(model).__name__, suffix)

    print("Training model", model_name)

    tb_writer = SummaryWriter(log_dir="log/{}".format(model_name))
    tb_writer.add_text("Optimizer", str(optimizer))
    tb_writer.add_text("Criterion", str(criterion))

    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_miou = 0.0

    waiting = 0
    graph = False
    for epoch in range(num_epochs):

        # Each epoch has a training and validation phase
        for phase in ['train', 'valid']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0

            # Iterate over data.
            tqdm_desc = "{}: Epoch {}/{} Loss: {:.4f} MIoU: {:.4f}"
            num_samples = 0
            m_miou = MIoU(num_classes=12, ignore_class=0)

            with tqdm(total=len(dataloaders[phase]), desc="") as pbar:
                for batch_num , sample in enumerate(dataloaders[phase]):
                    sample = sample2dev(sample, dev)

                    vox_tsdf = sample['vox_tsdf']
                    gt = sample['gt']

                    if not oracle:
                        vox_prior = sample['vox_prior']
                    else:
                        _, surf = torch.max(sample['vox_prior'], dim=1)
                        no_surf_idx = surf == 0
                        oracle_gt = gt.clone()
                        oracle_gt[no_surf_idx] = 0

                        #print("vp", sample['vox_prior'].shape)
                        #print("gt", gt.shape)
                        #print("oh_gt", torch.nn.functional.one_hot(gt.to(torch.int64), num_classes=12).shape)
                        vox_prior = torch.nn.functional.one_hot(oracle_gt.to(torch.int64), num_classes=12).to(
                            torch.float).transpose(3, 4).transpose(2,3).transpose(1,2)
                        #print("nvp",vox_prior.shape)

                    vox_weights = sample['vox_weights']

                    if torch.max(gt)>11:
                        print("maior")
                        continue
                    if torch.min(gt)<0:
                        print("menor")
                        continue



                    if not graph:
                        tb_writer.add_graph(model, (vox_tsdf, vox_prior))
                        graph = True

                    num_samples += gt.size(0)

                    # zero the parameter gradients
                    optimizer.zero_grad()

                    # forward
                    # track history if only in train
                    with torch.set_grad_enabled(phase == 'train'):
                        # Get model outputs and calculate loss
                        # Special case for inception because in training it has an auxiliary output. In train
                        #   mode we calculate the loss by summing the final output and the auxiliary output
                        #   but in testing we only consider the final output.
                        outputs = model(vox_tsdf, vox_prior)
                        loss = criterion(outputs, gt, vox_weights)

                        m_miou.update(outputs, gt, vox_weights)

                        # backward + optimize only if in training phase
                        if phase == 'train':
                            loss.backward()
                            optimizer.step()
                            #if type(scheduler) is optim.lr_scheduler.OneCycleLR:
                            #    scheduler.step()

                            # #########################################################################
                            #unsupervised
                            if alpha > 0 and batch_num % interval == (interval - 1):
                                sample = next(iter(unsup_dataloader))
                                sample = sample2dev(sample, dev)

                                vox_tsdf = sample['vox_tsdf']

                                _, unsup_labels = torch.max(sample['vox_prior'],dim=1)
                                no_surf_idx = unsup_labels==0
                                if not oracle:
                                    vox_prior = sample['vox_prior']
                                else:
                                    oracle_gt = gt.clone()
                                    oracle_gt[no_surf_idx]=0
                                    vox_prior = torch.nn.functional.one_hot(oracle_gt.to(torch.int64), num_classes=12).to(
                                        torch.float).transpose(3, 4).transpose(2, 3).transpose(1, 2)

                                vox_weights = sample['vox_weights']

                                # zero the parameter gradients
                                optimizer.zero_grad()

                                # forward
                                # track history if only in train
                                # Get model outputs and calculate unsup loss
                                outputs = model(vox_tsdf, vox_prior)
                                vox_weights[no_surf_idx]=0

                                loss2 = criterion(outputs, unsup_labels, vox_weights) * alpha

                                # backward + optimize only if in training phase
                                loss2.backward()
                                optimizer.step()

                            if type(scheduler) is optim.lr_scheduler.OneCycleLR:
                                scheduler.step()

                    # statistics
                    l = loss.item()
                    running_loss += l * gt.size(0)

                    pbar.set_description(tqdm_desc.format(phase, epoch + 1, num_epochs,
                                                          running_loss / num_samples,
                                                          m_miou.compute()
                                                          )
                                         )

                    pbar.update()

            epoch_loss = running_loss / num_samples
            epoch_miou = m_miou.compute()
            epoch_per_class_iou = m_miou.per_class_iou()

            tb_writer.add_scalar('Loss/{}'.format(phase), epoch_loss, epoch)
            tb_writer.add_scalar('mIoU/{}'.format(phase), epoch_miou, epoch)

            if phase == 'train':
                if type(scheduler) is not optim.lr_scheduler.OneCycleLR:
                    scheduler.step()
                for i, lr in enumerate(group['lr'] for group in optimizer.param_groups):
                    tb_writer.add_scalar('LR/{}'.format(i), lr, epoch)



            # deep copy the model
            if phase == 'valid' and epoch_miou > best_miou:
                waiting = 0
                print("mIoU improved from {:.5f} to  {:.5f}".format(best_miou,epoch_miou))
                best_epoch = epoch
                best_miou = epoch_miou
                best_per_class_iou = epoch_per_class_iou
                #best_model_wts = copy.deepcopy(model.state_dict())
                torch.save(model.state_dict(), "weights/{}_EPOCH_{}".format(model_name, epoch))

            elif phase == 'valid':
                print("mIoU {:.5f} was not an improvement from {:.5f}".format(epoch_miou,best_miou))
                eval
                waiting += 1

            if phase == 'valid':
                tb_text = ""
                for i in range(11):
                    text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * epoch_per_class_iou[i])
                    tb_text += text
                    print(text, end="        ")
                    if i % 4 == 3:
                        print()
                tb_writer.add_text("Per Class IoU", tb_text, global_step=epoch)
                print()
                time.sleep(.5)

        #torch.cuda.empty_cache()
        if waiting > patience:
            print("out of patience!!!")
            break

        # print()

    torch.save(model.state_dict(), "weights/{}_EPOCH_{}".format(model_name, epoch))

    time_elapsed = time.time() - since
    print(model_name)
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val MIoU%: {:6.1f}  Epoch: {}'.format(100 * best_miou, best_epoch))

    tb_text = ""
    for i in range(11):
        text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * best_per_class_iou[i])
        tb_text += text
        print(text, end="        ")
        if i % 4 == 3:
            print()
    tb_writer.add_text("Best Per Class IoU", tb_text)
    print()

    return model
